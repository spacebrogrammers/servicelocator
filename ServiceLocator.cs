using System;
using System.Collections.Generic;
using UnityEngine;

namespace com.SpaceBrogrammers.ServiceLocator
{
    /// <summary>
    /// Purpose of the ServiceLocator is to avoid multiple Singleton in project
    /// ServiceLocator will be in charge of store references and give the correct one
    /// when someone ask for it
    /// Each class that need to be accessed will need to Add itself to the ServiceLocator and
    /// remove itself when his lifecycle will end
    /// original: https://github.com/trashpandaboy/TPCore/blob/main/ServiceLocator.cs
    /// </summary>
    public class ServiceLocator : MonoBehaviour
    {
        protected static ServiceLocator instance;

        /// <summary>
        /// Contains all references to Services added
        /// </summary>
        Dictionary<Type, object> services = new Dictionary<Type, object>();

        public static ServiceLocator Get()
        {
            if (!instance)
            {
                GameObject go = new GameObject("ServiceLocator");
                instance = go.AddComponent<ServiceLocator>();
                DontDestroyOnLoad(go);
            }
            return instance;
        }

        public bool HasService<T>() where T : class
        {
            if(services.ContainsKey(typeof(T)))
            {
                if(services[typeof(T)] == null)
                {
                    //if the entry exist but the instance has been destroyed
                    services.Remove(typeof(T));
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Add the specified reference to the availables Services
        /// </summary>
        /// <typeparam name="T">Generic Type to handle multiple types</typeparam>
        /// <param name="serviceToAdd">The instance reference to add</param>
        public T AddService<T>(T serviceToAdd) where T : class
        {
            if (HasService<T>())
                return serviceToAdd;

            services[typeof(T)] = serviceToAdd;
            return serviceToAdd;
        }

        /// <summary>
        /// Remove the reference for specified type
        /// </summary>
        /// <typeparam name="T">Type of reference to remove</typeparam>
        public bool RemoveService<T>()
        {
            return services.Remove(typeof(T));
        }

        /// <summary>
        /// Retrieve the reference of specified type, if exist
        /// </summary>
        /// <typeparam name="T">Type of reference to retrieve</typeparam>
        /// <returns></returns>
        public T GetService<T>() where T : class
        {
            if (!HasService<T>())
                return null;

            return services[typeof(T)] as T;
        }

        /// <summary>
        /// Retrieve the reference of specified type if exist, otherwise create it
        /// </summary>
        /// <typeparam name="T">Type of reference to retrieve</typeparam>
        /// <returns></returns>
        public T GetOrCreateService<T>() where T : class
        {
            if (!HasService<T>())
                return AddService((T)Activator.CreateInstance(typeof(T)));

            return GetService<T>();
        }
    }
}